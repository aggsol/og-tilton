FROM gitpod/workspace-full

USER root
RUN sudo apt-get update

RUN sudo apt-get install -y \
    lcov \
    graphviz \
    pandoc \
    texlive \
    texlive-xetex \
    texlive-latex-extra \
    texlive-lang-german \
    texlive-fonts-extra \
    gpp \
    ccache \
    cppcheck \
    shelltestrunner \
	zzuf \
	todotxt-cli

RUN sudo apt-get autoremove -y