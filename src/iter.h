//  iter.h: interface for the Iter class.
//  2019-10-30

//  The Tilton Macro Processor

//  Douglas Crockford
//  http://www.crockford.com/tilton.html

//  This program is Open Source and Public Domain.

#ifndef __ITER_H_
#define __ITER_H_

class Text;

class Iter
{
public:
    Iter(Text*);
    virtual ~Iter();

    number     back();
    number     next();
    number     peek();

    number     character;
    number     index;
    number     line;
    Text*      text;
};


#endif //__ITER_H_
