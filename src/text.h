//  text.h: interface for the Text class.
//  2019-10-30

//  The Tilton Macro Processor

//  Douglas Crockford
//  http://www.crockford.com/tilton.html

//  This program is Open Source and Public Domain.

#ifndef __TEXT_H_
#define __TEXT_H_

#include "tilton.h"

class Context;

typedef void (*Op)(Context* context);

class Text {
public:
    Text();
    Text(number len);
    Text(const char* s);
    Text(const char* s, number len);
    Text(Text* t);
    virtual ~Text();

    void    append(number c);
    void    append(number c, number n);
    void    append(const char* s);
    void    append(const char* s, number len);
    void    append(Text* t);
    void    appendNumber(number);
    void    dump();
    number  get(number index);
    number  getNumber();
    number  hash();
    number  indexOf(Text* t);
    void    input();
    bool    is(const char* s);
    bool    is(Text* t);
    bool    isName(Text* t);
    number  lastIndexOf(Text* t);
    bool    lt(Text* t);
    void    output();
    bool    read(Text* t);
    void    set(number index, number c);
    void    set(Text* t);
    void    setName(const char* s);
    void    setName(const char* s, number len);
    void    setName(Text* t);
    void    substr(number start, number len);
    Text*   tail(number index);
    void    trim(Text* t);
    number  utfLength();
    Text*   utfSubstr(number start, number len);
    bool    write(Text* t);

    Op      function;
    number  length;
    Text*   link;       // hash collisions
    number  maxLength;
    char*   name;
    number  nameLength;
    char*   string;

private:
    void    checkMaxLength(number len);
    void    init(const char* s, number len);

    number  myHash;
};

#endif //__TEXT_H_
