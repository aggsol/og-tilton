//  node.h: interface for the node class.
//  2019-10-30

//  The Tilton Macro Processor

//  Douglas Crockford
//  http://www.crockford.com/tilton.html

//  This program is Open Source and Public Domain.

#ifndef __NODE_H_
#define __NODE_H_

#include "text.h"

class Text;

class Node {
public:
    Node(Text*);
    virtual ~Node();

    void    dump();

    Text*   text;
    Text*   value;
    Node*   node;
};

#endif // __NODE_H_
