//  context.h: interface for the context class.
//  2019-10-30

//  The Tilton Macro Processor

//  Douglas Crockford
//  http://www.crockford.com/tilton.html

//  This program is Open Source and Public Domain.

#ifndef __CONTEXT_H_
#define __CONTEXT_H_

#include "tilton.h"

class Iter;
class Text;
class Node;

class Context {
public:
    Context(Context*, Iter*);
    virtual ~Context();

    void    add(const char* s);
    void    add(Text*);
    void    dump();
    void    error(const char* reason, Text* evidence);
    void    error(const char* reason);
    void    eval(Text* input);
    Text*   evalArg(number argNr);
    Text*   evalArg(Node*);
    number  evalNumber(number argNr);
    number  evalNumber(Node*);
    Node*   getNode(number argNr);
    void    nop();
    void    resetArg(number argNr);

    Node*   node;
    Context* previous;

private:
    number  character;
    number  index;
    number  line;
    number  position;
    Iter*   source;
    Node*   last;
    void    whereError(Text* report);
};

#endif // __CONTEXT_H_
