//  node.cpp
//  2019-10-30

//  The Tilton Macro Processor

//  Douglas Crockford
//  http://www.crockford.com/tilton.html

//  This program is Open Source and Public Domain.

//  Node is used to make simple linked lists.

//  A node has three fields: text, value, and next.


#include <stdio.h>
#include "node.h"

Node::Node(Text* text)
{
    this->text = text;
    this->value = NULL;
    this->node = NULL;
}

Node::~Node(void)
{
    delete this->text;
    delete this->value;
    delete this->node;
}

void Node::dump()
{
    if (this->text) {
        fwrite(this->text->string, sizeof(char), this->text->length, stderr);
    }
    if (this->node) {
        fputc('~', stderr);
        this->node->dump();
    }
}