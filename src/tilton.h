//  tilton.h: constants for Tilton
//  2019-10-30

//  The Tilton Macro Processor

//  Douglas Crockford
//  http://www.crockford.com/tilton.html

//  This program is Open Source and Public Domain.

#ifndef __TILTON_H_
#define __TILTON_H_

#define EOT (-1)

// Tilton numbers are integers.

typedef long long number;

// INFINITY is the largest positive integer than can be held in a number.
// This value may be system dependent.

#define INFINITY ((number)0x7FFFFFFFFFFFFFFF)

// NAN (not a number) is the least necessary negative integer that can be held
// in a number. On two's compliment machines, it is INFINITY + 1. This value
// may be system dependent.

#define NAN ((number)0x8000000000000000)

#endif //__TILTON_H_
