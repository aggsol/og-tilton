//  iter.cpp: implementation of the Iter class.
//  2019-10-30

//  The Tilton Macro Processor

//  Douglas Crockford
//  http://www.crockford.com/tilton.html

//  This program is Open Source and Public Domain.

// Iter is just a convenient way of processing a text.
// It keeps track of lines for error messages.

#include <stdlib.h>
#include <stdio.h>
#include "tilton.h"
#include "text.h"
#include "iter.h"

Iter::Iter(Text* t) {
    text = t;
    line = 0;
    character = 0;
    index = 0;
}


Iter::~Iter() {
}


// back up one character.

number Iter::back() {
    if (text) {
        index -= 1;
        character -= 1;
        return text->get(index);
    }
    return EOT;
}


// return the next character.

number Iter::next() {
    if (text) {
        number c = text->get(index);
        index += 1;
        character += 1;
        if (c == '\n' || (c == '\r' && text->get(index) != '\n')) {
            line += 1;
            character = 0;
        }
        return c;
    }
    return EOT;
}


// peek ahead one character

number Iter::peek() {
    return (
        text
        ? text->get(index)
        : EOF
    );
}
